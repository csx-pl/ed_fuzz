<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<title>EditDistance - fuzz</title>
</head>
<body>
	<div class='container'>
		<div class="jumbotron">
			<h1 class="display-4">EditDistance - fuzz</h1>
			<p class="lead">Strona służy do procentowego obliczania miary EditDistance pomiędzy dwoma dostarczonymi tekstami.</p>
			<hr class="my-4">
			<p>Wykorzystane są dwa algorytmy: <strong>fuzz.ratio</strong> (bierze pod uwagę kolejność słów) oraz <strong>fuzz.token_sort_ratio</strong> (ignoruje zmiany w kolejności słów). Ostateczny wynik jest średnią ważoną obu metod.</p>
			<p>Algorytmy fuzz pochodzą z projektu: <a href='https://github.com/seatgeek/fuzzywuzzy'>fuzzywuzzy</a>.</p>
		</div>

		<form id="form">
			<div class='row'>
				<div class='col-md'>
					<div class="form-group">
						<label>Tekst 1</label>
						<textarea class="form-control" id="text1" rows="4"></textarea>
					</div>
				</div>

				<div class='col-md-2 text-center'>
					<input class="btn btn-primary btn-lg btn-block mt-5" type="submit" value="Oblicz">
				</div>

				<div class='col-md'>
					<div class="form-group">
						<label>Tekst 2</label>
						<textarea class="form-control" id="text2" rows="4"></textarea>
					</div>
				</div>
			</div>

			<div id='error' class='alert alert-danger' style='display: none;'></div>

			<div class='row mt-4'>
				<div class='col-md-5 text-right'>
					<div class="form-group row">
						<label class="col-6 col-form-label">fuzz.ratio</label>
						<div class="col-3">
							<input type="text" class="form-control" id='ed1' readonly>
						</div>
						<div class="col-3">
							<input type="number" class="form-control" id='ed1weight' step='1' max='100' min='0' value='80'>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-6 col-form-label">fuzz.token_sort_ratio</label>
						<div class="col-3">
							<input type="text" class="form-control" id='ed2' readonly>
						</div>
						<div class="col-3">
							<input type="number" class="form-control" id='ed2weight' step='1' max='100' min='0' value='20'>
						</div>
					</div>
				</div>

				<div class='col-md-3 offset-md-2'>
					<label>Obliczony Edit Distance</label>
					<input type="text" class="form-control form-control-lg text-primary" id='ed' readonly style='font-size: 3rem;'>
				</div>
			</div>

		</form>
	</div>

	<footer class="mt-5">
		<p class='text-muted text-center p-2'>&copy; <?= date('Y') ?> ArteQ</p>
	</footer>


	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-ZvpUoO/+PpLXR1lu4jmpXWu80pZlYUAfxl5NsBMWOEPSjUn/6Z/hRTt8+pR6L4N2" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/TypeWatch/3.0.1/jquery.typewatch.min.js"></script>
	<script>
		$(document).ready(function()
		{
			// calculate edit distance
			$("#form").submit(function(e)
			{
				$.ajax({
					url: "http://localhost:5000",
					method: 'POST',
					data: {
						text1: $("#text1").val(),
						text2: $("#text2").val(),
						w1: $("#ed1weight").val(),
						w2: $("#ed2weight").val()
					},
					dataType: "json",
					success: function(res)
					{
						if (res.status == "ok")
						{
							$("#error").hide();
							$("#ed1").val(res.ed1 + " %");
							$("#ed2").val(res.ed2 + " %");
							$("#ed").val(res.ed + " %");
						}
						else
						{
							$("#error").show().html(res.error);
						}
					},
					error: function()
					{
						alert("Ups, wystąpił jakiś błąd.");
					}
				});

				e.preventDefault();
			});

			// typewatch
			var options = {
			    callback: function (value){ $("#form").submit(); },
			    wait: 500,
			    highlight: true,
			    allowSubmit: false,
			    captureLength: 4
			}

			$("textarea").typeWatch( options );

			// change weight ratio
			$("#ed1weight").on("change", function()
			{
				let val = $(this).val();
				$("#ed2weight").val(100 - val);
				$("#form").submit();
			});

			$("#ed2weight").on("change", function()
			{
				let val = $(this).val();
				$("#ed1weight").val(100 - val);
				$("#form").submit();
			});
		});
	</script>

</body>
</html>