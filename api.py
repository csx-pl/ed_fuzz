from flask import Flask
from flask import request
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

app = Flask(__name__)

@app.route('/', methods=['POST'])
def calculate_ed():
    try:
        text1 = request.form['text1']
        text2 = request.form['text2']

        if not text1:
            raise Exception("Pusty tekst 1")
        if not text2:
            raise Exception("Pusty tekst 2")

        w1 = int(request.form['w1'])
        w2 = int(request.form['w2'])

        ed1 = fuzz.ratio(text1, text2)
        ed2 = fuzz.token_sort_ratio(text1, text2)
        ed = (w1 * ed1 + w2 * ed2) / 100

        return {"status": "ok", "ed1": ed1, "ed2": ed2, "ed": ed }
        
    except Exception as e:
        return {"status": "error", "error": str(e)} 

@app.after_request
def add_security_headers(resp):
    resp.headers['Content-Security-Policy']='default-src \'self\''
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
